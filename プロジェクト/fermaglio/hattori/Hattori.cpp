//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		Hattori.cpp
//-------------------------------------------------------------------------
#include "../GameCom.h"
#include "Hattori.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------


//Main.cppのGameInitで使用
void HATTORI::mGameInit(void) {

}
//GameMain.cppのGameAppで使用
void HATTORI::mGameUpdate(void) {

}

//Title.cpp
//TitleInitで使用
void HATTORI::mTitleInit(void) {

}
//TitleMainで使用
void HATTORI::mTitleMain(void) {

}
//TitleEndで使用
void HATTORI::mTitleEnd(void) {

}

//Select.cpp
//SelectInitで使用
void HATTORI::mSelectInit(void) {

}
//SelectMainで使用
void HATTORI::mSelectMain(void) {

}
//SelectEndで使用
void HATTORI::mSelectEnd(void) {

}

//Start.cpp
//StartInitで使用
void HATTORI::mStartInit(void) {

}
//StartMainで使用
void HATTORI::mStartMain(void) {

}
//StartEndで使用
void HATTORI::mStartEnd(void) {

}

//Stage.cpp
//StageInitで使用
void HATTORI::mStageInit(void) {

}
//StageMainで使用
void HATTORI::mStageMain(void) {

}
//Stagempdateで使用
void HATTORI::mStageUpdate(void) {

}
//StageEndで使用
void HATTORI::mStageEnd(void) {

}

//Resmlt.cpp
//ResmltInitで使用
void HATTORI::mResmltInit(void) {

}
//ResmltMainで使用
void HATTORI::mResmltMain(void) {

}
//ResmltEndで使用
void HATTORI::mResmltEnd(void) {

}
