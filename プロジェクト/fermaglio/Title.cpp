//-------------------------------------------------------------------------
//			3D Game
//			3DGameAtDxLib				2017.3
//			Source File		Title.cpp
//-------------------------------------------------------------------------
#pragma warning(disable:4996)
#pragma once
#include "GameCom.h"
#include "Lapping.h"
#include "Title.h"
#include "usui\Usui.h"
#include "hattori\Hattori.h"
#include "watanabe\Watanabe.h"
#include "nakada\Nakada.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------
//MenuElement Title[2] = {
//	{ 80, 200, "ゲーム開始" },
//	{ 100, 300, "終了" },
//};
//int SelectNum = 0;
//画像ハンドル
//int TitleHandle;

//-------------------------------------------------------------------------
//		初期化
//-------------------------------------------------------------------------
bool TitleInit(void) {

//	TitleHandle = LoadGraph("Image/Title2.png");
	usui.mTitleInit();
	hattori.mTitleInit();
	watanabe.mTitleInit();
	nakada.mTitleInit();
	return true;
}

//-------------------------------------------------------------------------
//		メイン
//-------------------------------------------------------------------------
void TitleMain(void){

	static int Init = TitleInit();
	usui.mTitleMain();
	hattori.mTitleMain();
	watanabe.mTitleMain();
	nakada.mTitleMain();

//	//画像の描画
//	DrawGraph(0, 0, TitleHandle, TRUE);

//	if (CheckKeyBoard(KEY_INPUT_DOWN) == 1){
//		SelectNum = (SelectNum + 1) % 2;
//		SelectSE.Play();
//	}
//	if (CheckKeyBoard(KEY_INPUT_UP) == 1){
//		SelectNum = (SelectNum + 1) % 2;
//		SelectSE.Play();
//	}
//	for (int i = 0; i < 2; i++){
//		if (i == SelectNum){
//			Title[i].x = 100;
//		}
//		else{
//			Title[i].x = 150;
//		}
//	}

//	for (int i = 0; i < 2; i++){
//		//DrawFormatString(Title[i].x, Title[i].y, COLOR_W, Title[i].name);
//		DrawStringToHandle(Title[i].x, Title[i].y, Title[i].name, COLOR_R, g_FontBIG);
//	}

//	if (CheckKeyBoard(KEY_INPUT_RETURN) == 1) {
//		EnterSE.Play();
//		switch (SelectNum) {
//		case 0:
//			//DrawString(XSIZE-200, YSIZE - 20, "Now Loading...", COLOR_W);
//			DrawStringToHandle(XSIZE - 400, YSIZE - 70, "Now Loading...", COLOR_R, g_FontBIG);
//			g_GameStatus = TAGGAME;
//			break;
//		case 1:
//			g_GameStatus = GAMEEND;
//			break;
//		}
//	}
}

//-------------------------------------------------------------------------
//		エンド
//-------------------------------------------------------------------------
void TitleEnd(void) {
	usui.mTitleEnd();
	hattori.mTitleEnd();
	watanabe.mTitleEnd();
	nakada.mTitleEnd();
}