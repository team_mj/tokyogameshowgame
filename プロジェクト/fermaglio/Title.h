//-------------------------------------------------------------------------
//			3D Game
//			3DGameAtDxLib				2017.3
//			Header File		Title.h
//-------------------------------------------------------------------------

//Titleのメイン
void TitleMain(void);

//Titleの初期化
bool TitleInit(void);

//Titleエンド
void TitleEnd(void);