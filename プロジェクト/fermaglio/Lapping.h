//-------------------------------------------------------------------------
//			3D Game
//			3DGameAtDxLib				2017.3
//			Header File		Lapping.h
//-------------------------------------------------------------------------
//ボタン作成
int CreateButton(int ButtonHandle, int Posx, int Posy, int Sizex, int Sizey, char *String);

//テキストボックス作成
void CreateTextBox(int x, int y, int x2, int y2, int Inputhandle);

//フレームレートの表示
void GetFps(int x, int y);
void GetFps(void);

//マウス・キーボードの押下関係
int CheckKeyBoard(int kcod);
int CheckMouse(int kcode);

//立方体のワイヤーフレームの描画
void DrawCube(VECTOR pos, VECTOR size);
void OBBDrawCube(VECTOR pos, VECTOR size);

//ランダムな値を出す
int GetRondom(int max, int min);
float GetRondom(float max, float min);