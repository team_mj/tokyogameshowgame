//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		GameMain.cpp
//-------------------------------------------------------------------------

#include "GameCom.h"
#include "Lapping.h"
#include "usui\Usui.h"
#include "hattori\Hattori.h"
#include "watanabe\Watanabe.h"
#include "nakada\Nakada.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
USUI usui;
HATTORI hattori;
WATANABE watanabe;
NAKADA nakada;
//-------------------------------------------------------------------------
//		ローカル変数
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
//		メイン処理に入る前の初期化
//-------------------------------------------------------------------------
void GameInit(void) {
	usui.mGameInit();
	hattori.mGameInit();
	watanabe.mGameInit();
	nakada.mGameInit();
}
//-------------------------------------------------------------------------
//		メイン処理
//-------------------------------------------------------------------------
void GameApp(void) {

	//ゲームループ
	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0) {
		//DrawString(0, 0, "Test", COLOR_W);
		//マウス座標の取得
		GetMousePoint(&MouseX, &MouseY);

		switch (g_GameStatus) {
		case TITLE:
			break;
		case SELECT:
			break;
		case GAME_START:
			break;
		case GAME_MAIN:
			break;
		case RESULT:
			break;
		case GAME_END:
			break;
		}
		usui.mGameUpdate();
		hattori.mGameUpdate();
		watanabe.mGameUpdate();
		nakada.mGamempdate();

#ifdef _DEBUG
		if (CheckHitKey(KEY_INPUT_F1) == 1)g_GameStatus = TITLE;
		if (CheckHitKey(KEY_INPUT_F2) == 1)g_GameStatus = GAME_START;
		if (CheckHitKey(KEY_INPUT_F3) == 1)g_GameStatus = GAME_MAIN;
		//if (CheckHitKey(KEY_INPUT_F4) == 1)g_GameStatus = LOADING;
		if (CheckHitKey(KEY_INPUT_F5) == 1)g_GameStatus = RESULT;
		//if (CheckHitKey(KEY_INPUT_F6) == 1)g_GameStatus = GAMEOVER;
		//if (CheckHitKey(KEY_INPUT_F7) == 1)g_GameStatus = STAGECNF;
		if (CheckHitKey(KEY_INPUT_F8) == 1)g_GameStatus = GAME_END;
#endif

		//ブレイク処理　ゲーム終了
		if (g_GameStatus == GAME_END || CheckHitKey(KEY_INPUT_ESCAPE) == 1)break;
		GetFps();
	}

}

//-------------------------------------------------------------------------
//		INT型の値の絶対値を返す
//-------------------------------------------------------------------------
int   Absolute(int Value) {
	if (Value < 0)Value *= -1;
	return Value;
}

//-------------------------------------------------------------------------
//		FLOAT型の値の絶対値を返す
//-------------------------------------------------------------------------
float Absolute(float Value) {
	if (Value < 0)Value *= -1;
	return Value;
}

//-------------------------------------------------------------------------
//		VECTOR型の値の絶対値を返す
//-------------------------------------------------------------------------
VECTOR Absolute(VECTOR Value) {
	if (Value.x < 0)Value.x *= -1;
	if (Value.y < 0)Value.y *= -1;
	if (Value.z < 0)Value.z *= -1;
	return VGet(Value.x, Value.y, Value.z);
}
//-------------------------------------------------------------------------
//		VECTOR型とFLOAT型の掛け算
//-------------------------------------------------------------------------
VECTOR VMulF(VECTOR v, float f) {
	return VGet(v.x*f, v.y*f, v.z*f);
}