//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Header File		Stage.h
//-------------------------------------------------------------------------
#pragma once

//Stageメイン
void StageMain(void);

//Stage初期化
bool StageInit(void);

//Stgaeアップデート
void StageUpdate(void);

//Stageエンド
void StageEnd(void);