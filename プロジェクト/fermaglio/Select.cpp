//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		Select.cpp
//-------------------------------------------------------------------------
#pragma once
#include "GameCom.h"
#include "Lapping.h"
#include "usui\Usui.h"
#include "hattori\Hattori.h"
#include "watanabe\Watanabe.h"
#include "nakada\Nakada.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
//		初期化
//-------------------------------------------------------------------------
bool SelectInit(void) {
	usui.mSelectInit();
	hattori.mSelectInit();
	watanabe.mSelectInit();
	nakada.mSelectInit();
	return true;
}

//-------------------------------------------------------------------------
//		メイン
//-------------------------------------------------------------------------
void SelectMain(void) {
	static int Init = SelectInit();
	usui.mSelectMain();
	hattori.mSelectMain();
	watanabe.mSelectMain();
	nakada.mSelectMain();
}

//-------------------------------------------------------------------------
//		エンド
//-------------------------------------------------------------------------
void SelectEnd(void) {
	usui.mSelectEnd();
	hattori.mSelectEnd();
	watanabe.mSelectEnd();
	nakada.mSelectEnd();
}