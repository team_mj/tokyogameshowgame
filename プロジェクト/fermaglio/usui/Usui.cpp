//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		usui.cpp
//-------------------------------------------------------------------------
#include "../GameCom.h"
#include "Usui.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------


//Main.cppのGameInitで使用
void USUI::mGameInit(void) {

}
//GameMain.cppのGameAppで使用
void USUI::mGameUpdate(void) {

}

//Title.cpp
//TitleInitで使用
void USUI::mTitleInit(void) {

}
//TitleMainで使用
void USUI::mTitleMain(void) {

}
//TitleEndで使用
void USUI::mTitleEnd(void) {

}

//Select.cpp
//SelectInitで使用
void USUI::mSelectInit(void) {

}
//SelectMainで使用
void USUI::mSelectMain(void) {

}
//SelectEndで使用
void USUI::mSelectEnd(void) {

}

//Start.cpp
//StartInitで使用
void USUI::mStartInit(void) {

}
//StartMainで使用
void USUI::mStartMain(void) {

}
//StartEndで使用
void USUI::mStartEnd(void) {

}

//Stage.cpp
//StageInitで使用
void USUI::mStageInit(void) {

}
//StageMainで使用
void USUI::mStageMain(void) {

}
//Stagempdateで使用
void USUI::mStageUpdate(void) {

}
//StageEndで使用
void USUI::mStageEnd(void) {

}

//Resmlt.cpp
//ResmltInitで使用
void USUI::mResmltInit(void) {

}
//ResmltMainで使用
void USUI::mResmltMain(void) {

}
//ResmltEndで使用
void USUI::mResmltEnd(void) {

}

//Start.cppのLoadStageFileで使用
void USUI::mLoadStageFile(void) {

}