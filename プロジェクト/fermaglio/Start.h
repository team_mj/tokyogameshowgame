//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Header File		Start.h
//-------------------------------------------------------------------------
#pragma once
//Startメイン
void StartMain(void);

//Start初期化
bool StartInit(void);

//Startエンド
void StartEnd(void);

//ステージファイルの読み込み
void LoadStageFile(void);
