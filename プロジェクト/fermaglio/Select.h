//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Header File		Select.h
//-------------------------------------------------------------------------
#pragma once

//Selectメイン
void SelectMain(void);

//Select初期化
bool SelectInit(void);

//Selectエンド
void SelectEnd(void);

