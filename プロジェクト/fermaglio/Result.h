//-------------------------------------------------------------------------
//			3D Game
//			3DGameAtDxLib				2017.3
//			Header File		Result.h
//-------------------------------------------------------------------------

//Resultメイン
void ResultMain(void);

//Result初期化
bool ResultInit(void);

//Resultエンド
void ResultEnd(void);