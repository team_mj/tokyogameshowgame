//-------------------------------------------------------------------------
//			3D Game
//			3DGameAtDxLib				2017.3
//			Source File		Lapping.cpp
//このファイルではDXライブラリの機能にないものをDXライブラリなどの関数を用いて
//ワンランクアップした関数を書いてあるところです。
//-------------------------------------------------------------------------
#pragma once
#include "GameCom.h"
#include "Lapping.h"

//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------
//FPS表示用変数
int mStaetTime = 0;		//Measurement Start
int mNowTime;			//Now Time
int mCount = 0;			//Counter
float mFps = 0;;			//Fps
const int N = 60;	//Average 
const int FPS = 60;	//Fps Configuration

//キー押下用変数
int kmode[256] = {};				//Key Mode
int Mcode[8] = {};

//テキストボックス用変数
int TextCnt = 0;
bool TextFlug = true;

//解説：DXライブラリでは"CheckHitKey"があるがそれだと"押されていない"か
//　　　"押されている"の二つしか判別できないのでゲーム性など増やすために
//		下記の関数を作成
//-------------------------------------------------------------------------
//		Check Key
//		kmode	0: 押してない
//				1: 押された
//				2: 押されている
//-------------------------------------------------------------------------
int CheckKeyBoard(int kcod) {

	if (CheckHitKey(kcod) == 0)kmode[kcod] = 0;
	if (CheckHitKey(kcod) == 1 && kmode[kcod] == 1)kmode[kcod] = 2;
	if (CheckHitKey(kcod) == 1 && kmode[kcod] == 0)kmode[kcod] = 1;

	return kmode[kcod];
}

//-------------------------------------------------------------------------
//		Mouse Button 
//		kmode	0: 押していない
//				1: 押された
//				2: 押されている
//-------------------------------------------------------------------------
int CheckMouse(int kcode) {
	if ((GetMouseInput() & kcode) == 0)Mcode[kcode] = 0;
	if ((GetMouseInput() & kcode) == 1 && Mcode[kcode] == 1)Mcode[kcode] = 2;
	if ((GetMouseInput() & kcode) == 1 && Mcode[kcode] == 0)Mcode[kcode] = 1;
	return Mcode[kcode];
}

//-------------------------------------------------------------------------
//		FPS設定
//		インターネット上にあったプログラムを参考にしました
//-------------------------------------------------------------------------
float GetFrameLate(void) {


	if (mStaetTime == 0) mStaetTime = GetNowCount();
	if (mCount == N) {
		mNowTime = GetNowCount();
		mFps = 1000.f / ((mNowTime - mStaetTime) / (float)N);
		mCount = 0;
		mStaetTime = mNowTime;
	}
	mCount++;
	return mFps;
}

//-------------------------------------------------------------------------
//		FPS表示（座標指定版）
//		int x : Screen Position X
//		int y : Screen Position Y
//-------------------------------------------------------------------------
void GetFps(int x, int y) {
	DrawFormatString(x, y, COLOR_W, "FPS: %.1f", GetFrameLate());
}

//-------------------------------------------------------------------------
//		FPS表示（座標指定なし）
//-------------------------------------------------------------------------
void GetFps(void) {
	DrawFormatString(5, YSIZE - 16, COLOR_W, "FPS: %.1f", GetFrameLate());
}


//解説：ボタンやテキストボックスを使おうとなるとWINAPIなどに頼るなどしなくては
//		ならないと思いだったら関数化しようと思い作りました。
//-------------------------------------------------------------------------
//		Button Create
//			Nomal:				0
//			Cursor On Button :	1
//			Button Click:		2
//-------------------------------------------------------------------------
int CreateButton(int ButtonHandle, int x, int y, int x2, int y2, char *String) {
	int StrWidth, StrLen;
	//下地となる黒い四角の描画
	DrawBox(x, y, x2, y2, COLOR_K, TRUE);
	//マウスが先ほど描画した黒い四角の中に入ってるか
	if ((MouseX > x && MouseX < x2) && (MouseY > y && MouseY < y2)) {
		//入っていたら明るめの灰色の四角を描画
		DrawBox(x + 1, y + 1, x2 - 1, y2 - 1, GetColor(150, 150, 150), TRUE);
		if (CheckMouse(MOUSE_INPUT_LEFT) == 1) {
			//マウスをクリックした瞬間暗めの灰色の四角を描画
			DrawBox(x + 1, y + 1, x2 - 1, y2 - 1, GetColor(100, 100, 100), TRUE);
			return 1;
		}
		//マウスをクリックしているとき暗めの灰色の四角を描画
		else if (CheckMouse(MOUSE_INPUT_LEFT) == 2) {
			DrawBox(x + 1, y + 1, x2 - 1, y2 - 1, GetColor(100, 100, 100), TRUE);
			return 2;
		}
	}
	else
		//それ以外なら灰色の四角を描画
		DrawBox(x + 1, y + 1, x2 - 1, y2 - 1, GetColor(200, 200, 200), TRUE);

	//文字列の長さを取得
	StrLen = (int)strlen(String);
	StrWidth = GetDrawStringWidth(String, StrLen);
	//ボタンの真ん中になるようにテキストを配置
	//DrawString((Sizex - Posx) / 2 - StrWidth / 2, (Sizey - Posy) / 2 - 5, String, COLOR_K);
	DrawString(((x2 - x) / 2 + x) - StrWidth / 2, (y + 1), String, COLOR_K);

	return 0;
}

//DXライブラリにある文字列描画関連を使ったテキストボックス
//-------------------------------------------------------------------------
//		Textbox Create
//-------------------------------------------------------------------------
void CreateTextBox(int x, int y, int x2, int y2, int Inputhandle) {
	TextCnt++;
	//カーソルの点滅処理
	if (TextCnt % 40 == 0) {
		if (TextFlug == false) {
			SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_NORMAL_CURSOR, COLOR_K);
			TextFlug = true;
		}
		else {
			SetKeyInputStringColor2(DX_KEYINPSTRCOLOR_NORMAL_CURSOR, COLOR_W);
			TextFlug = false;
		}
	}
	//テキストボックスの下地の作成
	DrawBox(x - 4, y - 4, x2 + 3, y2 + 3, COLOR_K, TRUE);
	DrawBox(x - 3, y - 3, x2 + 2, y2 + 2, COLOR_W, TRUE);
	//DXライブラリの関数を使い描画
	SetActiveKeyInput(Inputhandle);
	SetKeyInputDrawArea(x, y, x2, y2, Inputhandle);
	DrawKeyInputString(x, y, Inputhandle);

}

//立方体を描画する
//頂点番号の割り振り
//					5				　 6
//					─────────
//		　		　／│				／│
//				／　│			　／　│
//			　／	│			／	　│
//		4　　─────────　7　　│
//			│		│　　　　 │	　│
//			│		│		　 │	　│
//			│　  1 │──── │ ──│2
//			│　　 ／		　 │　　／   
//			│　 ／			　 │　／
//			│ ／			   │／
//			 ─────────
//			0					3
void DrawCube(VECTOR pos, VECTOR size){
	VECTOR Vertex[8];

	Vertex[0] = VGet(pos.x - size.x / 2,				pos.y - size.y / 2,				pos.z - size.z / 2);
	Vertex[1] = VGet(pos.x - size.x / 2,				pos.y - size.y / 2,				pos.z - size.z / 2 + size.z);
	Vertex[2] = VGet(pos.x - size.x / 2 + size.x,		pos.y - size.y / 2,				pos.z - size.z / 2 + size.z);
	Vertex[3] = VGet(pos.x - size.x / 2 + size.x,		pos.y - size.y / 2,				pos.z - size.z / 2);
	Vertex[4] = VGet(pos.x - size.x / 2,				pos.y - size.y / 2 + size.y,	pos.z - size.z / 2);
	Vertex[5] = VGet(pos.x - size.x / 2,				pos.y - size.y / 2 + size.y,	pos.z - size.z / 2 + size.z);
	Vertex[6] = VGet(pos.x - size.x / 2 + size.x,		pos.y - size.y / 2 + size.y,	pos.z - size.z / 2 + size.z);
	Vertex[7] = VGet(pos.x - size.x / 2 + size.x,		pos.y - size.y / 2 + size.y,	pos.z - size.z / 2);

	DrawLine3D(Vertex[0], Vertex[1], COLOR_G);
	DrawLine3D(Vertex[0], Vertex[3], COLOR_G);
	DrawLine3D(Vertex[0], Vertex[4], COLOR_G);

	DrawLine3D(Vertex[1], Vertex[2], COLOR_G);
	DrawLine3D(Vertex[1], Vertex[5], COLOR_G);

	DrawLine3D(Vertex[2], Vertex[3], COLOR_G);
	DrawLine3D(Vertex[2], Vertex[6], COLOR_G);

	DrawLine3D(Vertex[3], Vertex[7], COLOR_G);

	DrawLine3D(Vertex[4], Vertex[5], COLOR_G);
	DrawLine3D(Vertex[5], Vertex[6], COLOR_G);
	DrawLine3D(Vertex[6], Vertex[7], COLOR_G);
	DrawLine3D(Vertex[7], Vertex[4], COLOR_G);

}

void OBBDrawCube(VECTOR pos, VECTOR size){
	VECTOR Vertex[8];

	Vertex[0] = VGet(pos.x ,			pos.y ,					pos.z);
	Vertex[1] = VGet(pos.x ,			pos.y ,					pos.z + size.z/2);
	Vertex[2] = VGet(pos.x + size.x/2,	pos.y ,					pos.z + size.z/2);
	Vertex[3] = VGet(pos.x + size.x/2,	pos.y ,					pos.z);
	Vertex[4] = VGet(pos.x ,			pos.y + size.y/2,		pos.z);
	Vertex[5] = VGet(pos.x ,			pos.y + size.y/2,		pos.z + size.z/2);
	Vertex[6] = VGet(pos.x + size.x/2,	pos.y + size.y/2,		pos.z + size.z/2);
	Vertex[7] = VGet(pos.x + size.x/2,	pos.y + size.y/2,		pos.z);

	DrawLine3D(Vertex[0], Vertex[1], COLOR_G);
	DrawLine3D(Vertex[0], Vertex[3], COLOR_G);
	DrawLine3D(Vertex[0], Vertex[4], COLOR_G);

	DrawLine3D(Vertex[1], Vertex[2], COLOR_G);
	DrawLine3D(Vertex[1], Vertex[5], COLOR_G);

	DrawLine3D(Vertex[2], Vertex[3], COLOR_G);
	DrawLine3D(Vertex[2], Vertex[6], COLOR_G);

	DrawLine3D(Vertex[3], Vertex[7], COLOR_G);

	DrawLine3D(Vertex[4], Vertex[5], COLOR_G);
	DrawLine3D(Vertex[5], Vertex[6], COLOR_G);
	DrawLine3D(Vertex[6], Vertex[7], COLOR_G);
	DrawLine3D(Vertex[7], Vertex[4], COLOR_G);
}

//解説：DXライブラリには標準でランダムをだす関数GetRandがあるが最大値しか設定できず
//		範囲を指定したい場合には不向きだったため作成しました
//		参照ページ: http://9cguide.appspot.com/21-02.html
//-------------------------------------------------------------------------
//		ランダムな値を出す
//		max	: 最大値
//		min : 最小値
//-------------------------------------------------------------------------
int GetRondom(int max, int min) {
	return min + (int)(GetRand(99999)*(max - min + 1.0f) / (1.0f + RAND_MAX));
}
float GetRondom(float max, float min) {
	return min + (float)(GetRand(99999)*(max - min + 1.0f) / (1.0f + RAND_MAX));
}