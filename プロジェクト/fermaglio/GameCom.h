//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Header File		GameCom.h
//-------------------------------------------------------------------------
#include "DxLib.h"
//ゲームパロメータ
#define X_SIZE		1920
#define Y_SIZE		1080
//ゲームシーン
#define TITLE		0x00000001
#define GAME_START	0x00000002
#define GAME_MAINE	0x00000004
#define RESULT		0x00000008
#define GAME_END	0x00000016
//プレイヤーステータス
#define NORMAL		0x00000001
#define DAMAGE		0x00000002
#define WEIT		0x00000004
#define DEAD		0x00000008
//RGB
#define COLOR_W		(GetColor(255,255,255))
#define COLOR_K		(GetColor(  0,  0,  0))
#define COLOR_R		(GetColor(255,  0,  0))
#define COLOR_G		(GetColor(  0,255,  0))
#define COLOR_B		(GetColor(  0,  0,255))
