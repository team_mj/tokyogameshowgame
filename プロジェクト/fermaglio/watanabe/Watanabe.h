//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Header File		Watanabe.h
//-------------------------------------------------------------------------
#pragma once
class WATANABE {
public:
	//Main.cppのGameInitで使用
	void mGameInit(void);
	//GameMain.cppのGameAppで使用
	void mGameUpdate(void);

	//Title.cpp
	//TitleInitで使用
	void mTitleInit(void);
	//TitleMainで使用
	void mTitleMain(void);
	//TitleEndで使用
	void mTitleEnd(void);

	//Select.cpp
	//SelectInitで使用
	void mSelectInit(void);
	//SelectMainで使用
	void mSelectMain(void);
	//SelectEndで使用
	void mSelectEnd(void);

	//Start.cpp
	//StartInitで使用
	void mStartInit(void);
	//StartMainで使用
	void mStartMain(void);
	//StartEndで使用
	void mStartEnd(void);

	//Stage.cpp
	//StageInitで使用
	void mStageInit(void);
	//StageMainで使用
	void mStageMain(void);
	//Stagempdateで使用
	void mStageUpdate(void);
	//StageEndで使用
	void mStageEnd(void);

	//Resmlt.cpp
	//ResmltInitで使用
	void mResmltInit(void);
	//ResmltMainで使用
	void mResmltMain(void);
	//ResmltEndで使用
	void mResmltEnd(void);
};
