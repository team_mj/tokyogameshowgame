//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		Watanabe.cpp
//-------------------------------------------------------------------------
#include "../GameCom.h"
#include "Watanabe.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------


//Main.cppのGameInitで使用
void WATANABE::mGameInit(void) {

}
//GameMain.cppのGameAppで使用
void WATANABE::mGameUpdate(void) {

}

//Title.cpp
//TitleInitで使用
void WATANABE::mTitleInit(void) {

}
//TitleMainで使用
void WATANABE::mTitleMain(void) {

}
//TitleEndで使用
void WATANABE::mTitleEnd(void) {

}

//Select.cpp
//SelectInitで使用
void WATANABE::mSelectInit(void) {

}
//SelectMainで使用
void WATANABE::mSelectMain(void) {

}
//SelectEndで使用
void WATANABE::mSelectEnd(void) {

}

//Start.cpp
//StartInitで使用
void WATANABE::mStartInit(void) {

}
//StartMainで使用
void WATANABE::mStartMain(void) {

}
//StartEndで使用
void WATANABE::mStartEnd(void) {

}

//Stage.cpp
//StageInitで使用
void WATANABE::mStageInit(void) {

}
//StageMainで使用
void WATANABE::mStageMain(void) {

}
//Stagempdateで使用
void WATANABE::mStageUpdate(void) {

}
//StageEndで使用
void WATANABE::mStageEnd(void) {

}

//Resmlt.cpp
//ResmltInitで使用
void WATANABE::mResmltInit(void) {

}
//ResmltMainで使用
void WATANABE::mResmltMain(void) {

}
//ResmltEndで使用
void WATANABE::mResmltEnd(void) {

}
