//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		Stage.cpp
//-------------------------------------------------------------------------
#pragma once
#include "GameCom.h"
#include "Lapping.h"
#include "usui\Usui.h"
#include "hattori\Hattori.h"
#include "watanabe\Watanabe.h"
#include "nakada\Nakada.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
//		初期化
//-------------------------------------------------------------------------
bool StageInit(void) {
	usui.mStageInit();
	hattori.mStageInit();
	watanabe.mStageInit();
	nakada.mStageInit();
	return true;
}

//-------------------------------------------------------------------------
//		メイン
//-------------------------------------------------------------------------
void StageMain(void) {
	static int Init = StageInit();
	usui.mStageMain();
	hattori.mStageMain();
	watanabe.mStageMain();
	nakada.mStageMain();
}

//-------------------------------------------------------------------------
//		アップデート
//-------------------------------------------------------------------------
void StageUpdate(void) {
	usui.mStageUpdate();
	hattori.mStageUpdate();
	watanabe.mStageUpdate();
	nakada.mStageUpdate();
}

//-------------------------------------------------------------------------
//		エンド
//-------------------------------------------------------------------------
void StageEnd(void) {
	usui.mStageMain();
	hattori.mStageMain();
	watanabe.mStageMain();
	nakada.mStageMain();
}