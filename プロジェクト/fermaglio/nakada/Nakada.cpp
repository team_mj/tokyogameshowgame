//-------------------------------------------------------------------------
//			Project fermaglio
//			3DGameAtDxLib				2017.4
//			Source File		Nakada.cpp
//-------------------------------------------------------------------------
#include "../GameCom.h"
#include "Nakada.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------


//Main.cppのGameInitで使用
void NAKADA::mGameInit(void) {

}
//GameMain.cppのGameAppで使用
void NAKADA::mGameUpdate(void) {

}

//Title.cpp
//TitleInitで使用
void NAKADA::mTitleInit(void) {

}
//TitleMainで使用
void NAKADA::mTitleMain(void) {

}
//TitleEndで使用
void NAKADA::mTitleEnd(void) {

}

//Select.cpp
//SelectInitで使用
void NAKADA::mSelectInit(void) {

}
//SelectMainで使用
void NAKADA::mSelectMain(void) {

}
//SelectEndで使用
void NAKADA::mSelectEnd(void) {

}

//Start.cpp
//StartInitで使用
void NAKADA::mStartInit(void) {

}
//StartMainで使用
void NAKADA::mStartMain(void) {

}
//StartEndで使用
void NAKADA::mStartEnd(void) {

}

//Stage.cpp
//StageInitで使用
void NAKADA::mStageInit(void) {

}
//StageMainで使用
void NAKADA::mStageMain(void) {

}
//Stagempdateで使用
void NAKADA::mStageUpdate(void) {

}
//StageEndで使用
void NAKADA::mStageEnd(void) {

}

//Resmlt.cpp
//ResmltInitで使用
void NAKADA::mResmltInit(void) {

}
//ResmltMainで使用
void NAKADA::mResmltMain(void) {

}
//ResmltEndで使用
void NAKADA::mResmltEnd(void) {

}
