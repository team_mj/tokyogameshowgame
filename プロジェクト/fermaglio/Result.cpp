////-------------------------------------------------------------------------
////			3D Game
////			3DGameAtDxLib				2017.3
////			Source File		Demo.cpp
////-------------------------------------------------------------------------
#pragma once
#include "GameCom.h"
#include "Lapping.h"
#include "usui\Usui.h"
#include "hattori\Hattori.h"
#include "watanabe\Watanabe.h"
#include "nakada\Nakada.h"

//-------------------------------------------------------------------------
//		広域変数
//-------------------------------------------------------------------------
GLOBALVARIABLE
//extern int CoinCnt;
//extern bool dispMode;
//-------------------------------------------------------------------------
//		ファイル内ローカル変数
//-------------------------------------------------------------------------
//MenuElement Result[2] = {
//	{ 800, 300, "タイトルへ戻る" },
//	{ 1000, 400, "終了" },
//};
//int RSelectNum = 0;

//int ResultHandle;

//-------------------------------------------------------------------------
//		RESULTの初期化
//-------------------------------------------------------------------------
bool ResultInit(void) {
	usui.mResmltInit();
	hattori.mResmltInit();
	watanabe.mResmltInit();
	nakada.mResmltInit();
	//	ResultHandle = LoadGraph("Image/Result2.png");
	return true;
}

//-------------------------------------------------------------------------
//		RESULTメイン
//-------------------------------------------------------------------------
void ResultMain(void){

	static int Init = ResultInit();
	usui.mResmltMain();
	hattori.mResmltMain();
	watanabe.mResmltMain();
	nakada.mResmltMain();
//	//背景画像の描画
//	DrawGraph(0, 0, ResultHandle, TRUE);
//
//	if (CheckKeyBoard(KEY_INPUT_DOWN) == 1){
//		RSelectNum = (RSelectNum + 1) % 2;
//		SelectSE.Play();
//	}
//	if (CheckKeyBoard(KEY_INPUT_UP) == 1){
//		RSelectNum = (RSelectNum + 1) % 2;
//		SelectSE.Play();
//	}
//	for (int i = 0; i < 2; i++){
//		if (i == RSelectNum){
//			Result[i].x = 300;
//		}
//		else{
//			Result[i].x = 400;
//		}
//	}

//	//DrawFormatString(XSIZE / 2 - 100, 200, COLOR_W, "獲得したコインの枚数: %d", CoinCnt);
//	DrawFormatStringToHandle(XSIZE / 2 - 100, 200, COLOR_R, g_FontBIG, "獲得したコインの枚数: %d", CoinCnt);
//
//	for (int i = 0; i < 2; i++){
//		//DrawFormatString(Result[i].x, Result[i].y, COLOR_W, Result[i].name);
//		DrawStringToHandle(Result[i].x, Result[i].y, Result[i].name, COLOR_R, g_FontBIG);
//	}

//	if (CheckKeyBoard(KEY_INPUT_RETURN) == 1) {
//		EnterSE.Play();
//		switch (RSelectNum) {
//		case 0:
//			g_GameStatus = TITLE;
//			break;
//		case 1:
//			g_GameStatus = GAMEEND;
//			break;
//		}
//
//	}
}
//-------------------------------------------------------------------------
//		エンド
//-------------------------------------------------------------------------
void ResultEnd(void) {
	usui.mResmltEnd();
	hattori.mResmltEnd();
	watanabe.mResmltEnd();
	nakada.mResmltEnd();
}